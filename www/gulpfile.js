
'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var npmDist = require('gulp-npm-dist');

gulp.task('sass-css', function(){
  gulp.src('scss/serviceblick-global.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))

  gulp.src('scss/azia-vendor.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))

  return gulp.src('scss/azia-global.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('minify-css', function(){
  gulp.src('scss/serviceblick-global.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  gulp.src('scss/azia-vendor.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
  return gulp.src('scss/azia-global.scss')
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('serve', gulp.series('sass-css', () => {
  browserSync.init({
    server: true,
    open: false
  });
  gulp.watch('scss/**/*.scss',{interval: 100, usePolling: true}).on("change", gulp.series('minify-css'));
  gulp.watch('html/**/*.html',{interval: 100, usePolling: true}).on("change", browserSync.reload);
  gulp.watch('html/*.html',{interval: 100, usePolling: true}).on("change", browserSync.reload);
  gulp.watch('starters/*.html',{interval: 100, usePolling: true}).on("change", browserSync.reload);
  gulp.watch('js/*.js',{interval: 100, usePolling: true}).on("change", browserSync.reload);
}));

// Copy dependencies to lib/
gulp.task('npm-lib', async function() {
  gulp.src(npmDist(), {base:'/usr/src/node_modules/'})
    .pipe(rename(function(path) {
      path.dirname = path.dirname.replace(/\/dist/, '').replace(/\\dist/, '');
    }))
    .pipe(gulp.dest('lib'));
});
 