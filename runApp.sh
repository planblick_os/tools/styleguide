#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT

cd /usr/src/app
#npm run sass:watch &
gulp minify-css
gulp serve